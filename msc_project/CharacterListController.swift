//
//  CharacterListController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 30/06/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// The Item Structure that is used to add items to the character.
/// Properties:
///   - name: The name of the item.
///   - quantity: The amount of item.
///   - type: The category the item belongs. In this app it can be a Weapon, an Armor or an Adventuring Gear.
///   - isMagical: This property shows if the item has special magical features or not.
///   - description: This property holds the detailed descriptor of the item.
///   - modifierList: A dictionary array that holds names of character's ability scores as keys and his ability score modifiers as values.
struct Item {
    var name: String
    var quantity: Int
    var type : String
    var isMagical: Bool
    var description: String
    var modifierList: Dictionary<String,Int>
    init(_ chosenName: String = "", _ chosenQuantity: Int = 1, _ chosenType: String = "Adventuring Gear", _ isMagical: Bool = false, _ chosenDescription : String = "", _ chosenModifiers: Dictionary<String,Int> = [:]){
        self.name = chosenName
        self.quantity = chosenQuantity
        self.type = chosenType
        self.isMagical = isMagical
        self.description = chosenDescription
        self.modifierList = chosenModifiers
    }
}

/// The Spell Structure that is used to add spells to the character.
/// Properties:
///   - name: Spell's name.
///   - time: The time required to cast the spell.
///   - type: The school of magic the spell belongs to.
///   - range: The effective range of the spell.
///   - duration: The period of time the spell stands active.
///   - description: Detailed description of the spell.
///   - diceRolls: This tuple array holds dice roll elements that include the numberOfDice and numberOfDice side. These dice rolls
/// can be used for various reasons by different spells.
///   - ability: The name of the ability score the character uses to cast the spell.
struct Spell {
    var name: String
    var time: String
    var type: String
    var range: String
    var duration: String
    var description: String
    var diceRolls: [(numberOfDice: Int,numberOfDiceSide: Int)]
    var ability: String
    init(_ chosenName: String = "", _ chosenTime: String = "", _ chosenType: String = "", _ chosenRange: String = "", _ chosenDuration: String = "", _ chosenDescription: String = "", _ chosenDiceRolls: [(numberOfDice: Int,numberOfDiceSide: Int)] = [], _ chosenAbility: String = "")
    {
        self.name = chosenName
        self.time = chosenTime
        self.type = chosenType
        self.range = chosenRange
        self.duration = chosenDuration
        self.description = chosenDescription
        self.diceRolls = chosenDiceRolls
        self.ability = chosenAbility
    }
}

/// The Attack Structure that is used to add attacks to the character.
/// Properties:
///   - name: The name of the attack.
///   - type: This property tells if the attack is melee or ranged as the attack typing is generally this way in many
/// tabletop RPG game. It can also be used for a different attack typing for a different system as well
///   - attackRoll: This tuple is made of two numbers: the number of dice and the number of dice side to form a dice roll.
/// this roll is used to check if the attack landed on the target or not.
///   - damageRolls: This tuple array holds damage roll elements that include the numberOfDice and numberOfDice side. These dice rolls
/// are used for damage calculation of the attack.
///   - ability: The name of the ability score the character uses to cast the spell.
///   - damageType: The type of damage the attack inflicted. Generally it would be slashing, piercing or bludgeoning.
///   - ability: The name of the ability score the character uses to perform the attack.
struct Attack {
    var name: String
    var type: String
    var attackRoll: (numberOfDice: Int,numberOfDiceSide: Int)
    var damageRolls: [(numberOfDice: Int,numberOfDiceSide: Int)]
    var damageType: String
    var ability: String
    init(_ chosenName: String = "", _ chosenType: String = "", _ chosenAttackRoll:  (numberOfDice: Int,numberOfDiceSide: Int) = (numberOfDice : 0, numberOfDiceSide : 0), _ chosenDamageRolls: [(numberOfDice: Int,numberOfDiceSide: Int)] = [], _ chosenDamageType: String = "", _ chosenAbility: String = "")
    {
        self.name = chosenName
        self.type = chosenType
        self.attackRoll = chosenAttackRoll
        self.damageRolls = chosenDamageRolls
        self.damageType = chosenDamageType
        self.ability = chosenAbility
    }
}

/// The Note Structure that is used to add notes to the character sheet's Notes tab page.
/// Properties:
///   - title: The title of the note.
///   - body: The body of the note.
struct Note {
    var title: String
    var body : String
    init(_ chosenTitle: String = "", _ chosenBody: String = ""){
        self.title = chosenTitle
        self.body = chosenBody
    }
}

/// The Character Structure that is used to create characters to use in tabletop RPG gameplay.
/// Properties:
///   - charClass: The character's class.
///   - race: The chosen race of the character.
///   - name: The name of the character.
///   - abilityScoreList: This property is a dictionary holds the names of the ability scores the 
/// character has as keys and their numbers as values.
///   - itemList: This array is made of Item struct instance elements and stores the items of the 
/// character.
///   - spellList: This array is made of Spell struct instance elements and stores the spells the 
/// character can cast.
///   - attackList: This array is made of Attack struct instance elements and stores the attacks 
/// the character can perform.
///   - noteList: This array is holds Note struct instances and stores the notes located on the 
/// notes tab page of the character sheet.
///   - charImage: The reference image of the character.
struct Character {
    var charClass: String
    var race: String
    var name: String
    var abilityScoreList: Dictionary<String,Int>
    var itemList: [Item]
    var spellList: [Spell]
    var attackList: [Attack]
    var noteList: [Note]
    var charImage: UIImage
    init(_ chosenClass: String = "", _ chosenRace: String = "", _ chosenName: String = "",_ abilityScores: Dictionary<String,Int> = [:], _ chosenItems: [Item] = [],_ chosenSpells: [Spell] = [], _ chosenAttacks:[Attack] = [], _ chosenNotes: [Note] = [], _ chosenImage: UIImage = #imageLiteral(resourceName: "default_character_image"))
    {
        self.charClass = chosenClass
        self.race = chosenRace
        self.name = chosenName
        self.abilityScoreList = abilityScores
        self.itemList = chosenItems
        self.spellList = chosenSpells
        self.attackList = chosenAttacks
        self.noteList = chosenNotes
        self.charImage = chosenImage
    }
}

/// The list of characters that are stored in the app. Two already made characters
/// exist for demonstration of the prototype.
var myCharacters :[Character] = [Character("Bard", "Half-Elf", "Davros",["Strength":10,"Dexterity":16,"Intelligence":20],
                                           [Item("Rapier", 1, "Weapon", false, "This rapier belonged to Davros' teacher.",["Dexterity":1]),
                                            Item("Leather Armor", 1, "Armor", false, "This is a standard leather armor.",["Dexterity":1]),
                                            Item("Lute", 1, "Adventuring Gear", true, "This magical instrument helps Davros cast spells",["Intelligence":1])],[Spell("Fireball", "1 Action", "Evocation", "120 ft", "Instantaneous", "A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren't being worn or carried.", [(numberOfDice: 8, numberOfDiceSide: 6)], "Intelligence"),Spell("Faerie Fire", "1 Action", "Conjuration", "120 ft", "Instantaneous", "A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren't being worn or carried.", [(numberOfDice: 8, numberOfDiceSide: 6)], "Intelligence"),Spell("Hold Person", "1 Action", "Evocation", "120 ft", "Instantaneous", "A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren't being worn or carried.", [(numberOfDice: 8, numberOfDiceSide: 6)], "Intelligence"),Spell("Ice Cone", "1 Action", "Evocation", "120 ft", "Instantaneous", "A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren't being worn or carried.", [(numberOfDice: 8, numberOfDiceSide: 6)], "Intelligence"),Spell("Glyphic Shield", "1 Action", "Abjuration", "120 ft", "Instantaneous", "A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren't being worn or carried.", [(numberOfDice: 8, numberOfDiceSide: 6)], "Intelligence")],
                                                [Attack("Rapier", "Melee",(numberOfDice: 1, numberOfDiceSide: 20),[(numberOfDice: 1, numberOfDiceSide: 8)], "Piercing","Dexterity"),
                                                Attack("Flurry", "Melee",(numberOfDice: 1, numberOfDiceSide: 20),[(numberOfDice: 1, numberOfDiceSide:8)],"Piercing","Dexterity"),
                                                Attack("Poetic Slash", "Melee",(numberOfDice: 1, numberOfDiceSide: 20),[(numberOfDice: 1, numberOfDiceSide: 8)], "Piercing","Dexterity")],
                                                [Note("College of Lore", "Bards of the College of Lore know something about most things, collecting bits of knowledge from sources as diverse as scholarly tomes and peasant tales. Whether singing folk ballads in taverns or elaborate compositions in royal courts, these bards use their gifts to hold audiences spellbound. When the applause dies down, the audience members might find themselves questioning everything they held to be true, from their faith in the priesthood of the local temple to their loyalty to the king. The loyalty of these bards lies in the pursuit of beauty and truth, not in fealty to a monarch or following the tenets of a deity. A noble who keeps such a bard as a herald or advisor knows that the bard would rather be honest than politic. The college’s members gather in libraries and sometimes in actual colleges, complete with classrooms and dormitories, to share their lore with one another. They also meet at festivals or affairs of state, where they can expose corruption, unravel lies, and poke fun at self-important figures of authority."),
                                                 Note("Countercharm","You have the ability to use musical notes or words of power to disrupt mind-influencing effects. As an action, you can start a performance that lasts until the end of your next turn. During that time, you and any friendly creatures within 30 feet of you have advantage on saving throws against being Frightened or Charmed. A creature must be able to hear you to gain this benefit. The performance ends early if you are Incapacitated or silenced or if you voluntarily end it (no action required).")],
                                                #imageLiteral(resourceName: "davros")),
                                 Character("Rogue", "Tiefling", "Ya'ara",["Might":12,"Agility":18,"Will":14],[Item("Dagger", 2, "Weapon", false, "An iron dagger decorated with ornate flower figures on the blade and on the handle.",["Agility":2])],[Spell("Darkness", "1 Action", "Evocation", "60 ft", "Up to 10 minutes", "Magical darkness spreads from a point you choose within range to fill a 15-foot radius Sphere for the Duration. The darkness spreads around corners. A creature with Darkvision can't see through this darkness, and nonmagical light can't illuminate it. If the point you choose is on an object you are holding or one that isn't being worn or carried, the darkness emanates from the object and moves with it. Completely covering the source of the darkness with an opaque object, such as a bowl or a helm, blocks the darkness.", [], "Will")], [Attack("Dagger", "Melee",(numberOfDice: 1, numberOfDiceSide: 20),[(numberOfDice: 1, numberOfDiceSide: 4)], "Piercing","Agility"), Attack("Sneak Attack", "Melee",(numberOfDice: 1, numberOfDiceSide: 20),[(numberOfDice: 4, numberOfDiceSide: 6)], "Piercing","Agility")], [Note("Darkvision","Thanks to your infernal heritage, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in Darkness as if it were dim light. You can’t discern color in Darkness, only shades of gray."), Note("Hellish Resistance","You have resistance to fire damage."), Note("Thieves' Cant","During your rogue Training you learned thieves' cant, a secret mix of dialect, jargon, and code that allows you to hide messages in seemingly normal conversation. Only another creature that knows thieves' cant understands such messages. It takes four times longer to convey such a Message than it does to speak the same idea plainly. In addition, you understand a set of secret signs and symbols used to convey short, simple messages, such as whether an area is dangerous or the territory of a thieves' guild, whether loot is nearby, or whether the people in an area are easy marks or will provide a safe house for thieves on the run.")], #imageLiteral(resourceName: "yaara"))]

/// This class controls the Character List View in the app. It lists the created characters
/// and takes the user to the character sheet and character creation features.
class CharacterListController: UITableViewController {

    
    
    var newCharacter = Character()
    var chosenCharacter = Character()
    var didCreateNewCharacter = false

    @IBOutlet weak var addButton: UIBarButtonItem!
    
    ///This function activates when the user taps the Add button on top right corner of
    /// the navigation bar. It opens up an alert asking which kind of character
    /// the user wants to create.
    @IBAction func addButtonClicked(_ sender: Any) {

        let alert = UIAlertController(title: "Create Character", message: "Which kind of character do you want to create?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "D&D Character", style: UIAlertActionStyle.default, handler: {(action)in self.performSegue(withIdentifier: "goToClassSelect", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Custom Character", style: UIAlertActionStyle.default, handler: {(action)in self.performSegue(withIdentifier: "goToCustomAbility", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /// This function transfers the chosen character information to the character sheet to be
    /// presented to the user.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCharacterSheet" {
            let destinatedController : TabBarController = segue.destination as! TabBarController
            destinatedController.chosenCharacter = self.chosenCharacter
        }
    }
    
    /// This function does necessary changes before the View is interacted by the user.
    /// It unhides the navigation bar and hides the default Back button on it. This is done as the user
    /// taps the back button in the character list view, it should always take the user to
    /// the intro scene. So, a custom back button is made instead in its place.
    /// The character list updates are done here as well.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Home", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CharacterListController.goBackHome(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        self.navigationController?.isNavigationBarHidden = false
        if(didCreateNewCharacter)
        {
            didCreateNewCharacter = false
            myCharacters.append(newCharacter)
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        }
    }
    
    ///This function takes activates when the user taps the custom back button.
    /// It takes the user to the intro scene.
    func goBackHome(sender: UIBarButtonItem) {
        performSegue(withIdentifier: "goToIntro", sender: navigationItem.backBarButtonItem)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCharacters.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CharacterListCell
        let characterObject = myCharacters[indexPath.row]
        cell.characterImageView.image = characterObject.charImage
        cell.nameLabel.text = characterObject.name
        cell.classLabel.text = characterObject.charClass
        return cell
    }

    /// Selecting a cell in the character list takes the user to the character sheet.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.chosenCharacter = myCharacters[indexPath.row]
        self.performSegue(withIdentifier: "goToCharacterSheet", sender: indexPath);
    }
}
