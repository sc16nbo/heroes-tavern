//
//  SpellSelectController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 18/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit


/// This class controls the Attack and Spell Selection List View. The user can add any number of attacks or spells
/// to the character instance that is in the creation process. The creation and edition of attacks and spells are done
/// in other respective views. They are accessed through this class. The created elements are listed on the view of
/// this class.
class SpellSelectController: UITableViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var createSpellButton: UIBarButtonItem!
    
    var newCharacter = Character()
    var sectionNames = ["Attacks","Spells"]
    var selectedAttackSpellIndex = 0
    
    /// This action function belongs to the Add button on top right of the user interface. It brings out alert that
    /// takes the user to the relevant view to create an attack or a spell.
    @IBAction func createSpellButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Choose What to Add", message: "Would you like to add an attack or a spell?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Attack", style: UIAlertActionStyle.default, handler: {(action)in self.performSegue(withIdentifier: "goToCreateAttack", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Spell", style: UIAlertActionStyle.default, handler: {(action)in self.performSegue(withIdentifier: "goToCreateSpell", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    ///This action function belongs to the Next button on the UI. When tapped,
    /// it takes the user to the next step of character creation, Name and Image Selection.
    @IBAction func nextButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "goToSelectNameImage", sender: nextButton)
    }
    
    
    /// This function is used to unwind any segues that has been performed.
    /// While doing so, necessary changes are performed according to which
    /// view the app unwinds from.
    @IBAction func unwind(segue:UIStoryboardSegue) {
        if let sourceViewController = segue.source as? CreateSpellController {

            if newCharacter.spellList.count != sourceViewController.newCharacter.spellList.count{
                self.newCharacter = sourceViewController.newCharacter
            self.tableView.beginUpdates()
            let IndexPathOfLastRow = NSIndexPath(row: self.newCharacter.spellList.count - 1, section: 1)
            self.tableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
            self.tableView.reloadData()
            self.tableView.endUpdates()
            }
            else
            {
                self.newCharacter = sourceViewController.newCharacter
                self.tableView.beginUpdates()
                self.tableView.reloadData()
                self.tableView.endUpdates()
            }

        }
        else if let sourceViewController = segue.source as? CreateAttackController{
            if newCharacter.attackList.count != sourceViewController.newCharacter.attackList.count{
                self.newCharacter = sourceViewController.newCharacter
                self.tableView.beginUpdates()
                let IndexPathOfLastRow = NSIndexPath(row: self.newCharacter.attackList.count - 1, section: 0)
                self.tableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
                self.tableView.reloadData()
                self.tableView.endUpdates()
            }
            else
            {
                self.newCharacter = sourceViewController.newCharacter
                self.tableView.beginUpdates()
                self.tableView.reloadData()
                self.tableView.endUpdates()
            }

        }

    }
    
    /// Passing data with the segue to a destinated controller that will use it.
    /// This function also makes necessary changes in the variables in destinated controller.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToEditSpell"
        {
            let destinatedController : CreateSpellController = segue.destination as! CreateSpellController
            destinatedController.newCharacter = newCharacter
            destinatedController.newSpell = newCharacter.spellList[selectedAttackSpellIndex]
            destinatedController.selectedSpellIndex = selectedAttackSpellIndex
            destinatedController.createButtonTitle = "Finish Editing Spell"
            
        }
        else if segue.identifier == "goToCreateSpell"
        {
            let destinatedController : CreateSpellController = segue.destination as! CreateSpellController
            destinatedController.newCharacter = newCharacter
            print("gotocreatespell")
            print(newCharacter.abilityScoreList)
            destinatedController.createButtonTitle = "Create Spell"
        }
        else if segue.identifier == "goToCreateAttack"{
            let destinatedController : CreateAttackController = segue.destination as! CreateAttackController
            destinatedController.newCharacter = newCharacter
            destinatedController.createButtonTitle = "Create Attack"
        }
        else if segue.identifier == "goToEditAttack"{
           let destinatedController : CreateAttackController = segue.destination as! CreateAttackController
            destinatedController.newCharacter = newCharacter
            destinatedController.newAttack = newCharacter.attackList[selectedAttackSpellIndex]
            destinatedController.selectedAttackIndex = selectedAttackSpellIndex
            destinatedController.createButtonTitle = "Finish Editing Attack"
        }
        else if segue.identifier == "goToSelectNameImage"{
            let destinatedController : CharacterNameImageController = segue.destination as! CharacterNameImageController
            destinatedController.newCharacter = newCharacter
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sectionNames.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionNames[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return newCharacter.attackList.count
        }
        else {
            return newCharacter.spellList.count
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttackSpellCell", for: indexPath) as! AttackSpellCell
        if indexPath.section == 0{
            let attackSpellObject = newCharacter.attackList[indexPath.row]
            cell.nameLabel.text = "\(attackSpellObject.name)"
            cell.typeLabel.text = "\(attackSpellObject.type)"
        }
        else{
            let attackSpellObject = newCharacter.spellList[indexPath.row]
            cell.nameLabel.text = "\(attackSpellObject.name)"
            cell.typeLabel.text = "\(attackSpellObject.type)"
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAttackSpellIndex = indexPath.row
        if indexPath.section == 0{
            performSegue(withIdentifier: "goToEditAttack", sender: UITableViewCell.self)
        }
        else{
            performSegue(withIdentifier: "goToEditSpell", sender: UITableViewCell.self)
        }
        
    }

}
