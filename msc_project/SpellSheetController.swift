//
//  SpellSheetController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 26/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class is in charge of the Spells tab page of the character sheet. It lists the spells'
/// summaries in a TableView object, and the user can reach to the detailed view of a spell (Spell Pop Up)
/// from the view being controlled by this class.
class SpellSheetController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var chosenCharacter = Character()
    var chosenSpell : Spell?
    
    @IBOutlet weak var spellSheetTableView: UITableView!
    @IBOutlet weak var addSpellButton: UIBarButtonItem!
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.tabBarController?.navigationItem.rightBarButtonItem = addSpellButton;
        
    }
    
    /// This function sets protocols for the spell sheet table view and hiding empty table cells
    override func viewDidLoad() {
        super.viewDidLoad()
        spellSheetTableView.delegate = self
        spellSheetTableView.dataSource = self
        spellSheetTableView.tableFooterView = UIView(frame: .zero)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return chosenCharacter.spellList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpellSheetCell", for: indexPath) as! AttackSpellCell
        
            let spellObject = chosenCharacter.spellList[indexPath.row]
            cell.spellSheetNameLabel.text = "\(spellObject.name)"
            cell.spellSheetTypeLabel.text = "\(spellObject.type)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        chosenSpell = chosenCharacter.spellList[indexPath.row]
        performSegue(withIdentifier: "goToSpellPopUp", sender: self)
    }
    
    /// This function does the necessary ability modifier calculation and transfers it with the chosen spell from the list to the spell pop up window.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSpellPopUp"{
            let destinatedController : SpellPopUpController = segue.destination as! SpellPopUpController
           let abilityModifier = Int((chosenCharacter.abilityScoreList[(chosenSpell?.ability)!]! - 10 ) / 2)
            destinatedController.chosenSpell = chosenSpell!
            destinatedController.abilityModifier = abilityModifier
        }
    }
}
