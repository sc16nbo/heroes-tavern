//
//  CreateSpellController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 18/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit


/// This class controls the spell creation and edition view of the character creation process. It has interface elements to get
/// the necessary information of a spell from the user and saves it to the spellList variable array of the newCharacter instance.
class CreateSpellController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    var newCharacter = Character()
    var newSpell = Spell()
    var selectedSpellIndex = 0
    var createButtonTitle : String = ""
    var diceRollArray : [(numberOfDice: Int,numberOfDiceSide: Int)] = []
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var spellName: UITextField!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var spellType: UITextField!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var spellTime: UITextField!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var spellRange: UITextField!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var spellDuration: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var spellDescription: UITextView!
    @IBOutlet weak var abilityLabel: UILabel!
    @IBOutlet weak var spellAbilityPicker: UIPickerView!
    @IBOutlet weak var diceLabel: UILabel!
    @IBOutlet weak var diceRollTableView: UITableView!
    @IBOutlet weak var addDiceButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    
    /// This function takes the information entered by the user and saves them to the Character instance's spellList variable array.
    /// If it is a new spell, it appends to the list, and if it is an editing process, it changes the previous version of the element
    /// in the array. Then it unwinds the segue, returning back to the Attacks and Spells List View of the character creation process.
    @IBAction func createButtonClicked(_ sender: Any) {
        newSpell.name = spellName.text!
        newSpell.type = spellType.text!
        newSpell.time = spellTime.text!
        newSpell.range = spellRange.text!
        newSpell.duration = spellDuration.text!
        newSpell.description = spellDescription.text
        let abilityNameArray = newCharacter.abilityScoreList.keys.sorted()
        newSpell.ability = abilityNameArray[spellAbilityPicker.selectedRow(inComponent: 0)]
        newSpell.diceRolls = diceRollArray
        if createButtonTitle == "Create Spell"
        {
            newCharacter.spellList.append(newSpell)
        }
        else
        {
            newCharacter.spellList[selectedSpellIndex] = newSpell
        }
        performSegue(withIdentifier: "unwindToSpellList", sender: createButton)
    }

    /// This function initiates the outlet values (empty if its a creation view or the previous data
    /// if it is an edition view), writes the placeholder texts for text field objects and gets necessary
    /// information from the new or chosen spell.
    override func viewDidLoad() {
        super.viewDidLoad()
        spellName.text = newSpell.name
        spellType.text = newSpell.type
        spellTime.text = newSpell.time
        spellRange.text = newSpell.range
        spellDuration.text = newSpell.duration
        spellDescription.text = newSpell.description
        createButton.setTitle(createButtonTitle, for: .normal)
        if createButtonTitle != "Create Spell"
        {
            let abilityNameArray = newCharacter.abilityScoreList.keys.sorted()
        spellAbilityPicker.selectRow(abilityNameArray.index(of: newSpell.ability)!
, inComponent: 0, animated: false)
        }
        diceRollArray = newSpell.diceRolls
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return newCharacter.abilityScoreList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let abilityScoreNameArray = self.newCharacter.abilityScoreList.keys.sorted()
            return abilityScoreNameArray[row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return diceRollArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "diceRollCell", for: indexPath) as! DiceRollCell
        cell.diceRollLabel.text = "\(diceRollArray[indexPath.row].numberOfDice)  d\(diceRollArray[indexPath.row].numberOfDiceSide)"
        return cell
    }
    
    /// This is the action function of the Add Dice Roll Button. It brings up
    /// an alert with a title, message, two text fields for getting input and finally
    /// Done and Cancel buttons. It gets the entered dice number and side information,
    /// and adds the taken data to the diceRollTableView in the Create Spell View.
    @IBAction func addDiceRoll(_ sender: Any) {
        let alert = UIAlertController(title: "Add Dice Roll", message: "Select number and side of dice", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (diceNumberTextField) in diceNumberTextField.placeholder = "Number Of Dice"}
        alert.addTextField{ (diceSideTextField) in diceSideTextField.placeholder = "Number Of Sides of One Dice"}
        
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let diceNumberTextField = alert.textFields![0]
            let diceSideTextField = alert.textFields![1]
            self.diceRollArray.append((numberOfDice: Int(diceNumberTextField.text!)!, numberOfDiceSide: Int(diceSideTextField.text!)!))
            self.diceRollTableView.beginUpdates()
            let IndexPathOfLastRow = NSIndexPath(row: self.diceRollArray.count - 1, section: 0)
            self.diceRollTableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
            self.diceRollTableView.reloadData()
            self.diceRollTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /// This function is called when the user taps an already existing dice roll on
    /// the diceRollTableView. It brings up an alert with a title, message, two
    /// editable text fields with the already existing information and finally
    /// Done and Cancel buttons. When clicked to Done button, it gets the changed
    /// dice number and side information and updates the respective cell data on the
    /// diceRollTableView.
    ///
    /// - Parameters:
    ///   - row: The row number of the chosen cell on the damageRollTableView.
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func editDiceRoll (row: Int, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (diceNumberTextField) in diceNumberTextField.placeholder = "Number Of Dice"
            diceNumberTextField.text = "\(self.diceRollArray[row].numberOfDice)"
        }
        alert.addTextField{ (diceSideTextField) in diceSideTextField.placeholder = "Number Of Sides of One Dice"
            diceSideTextField.text = "\(self.diceRollArray[row].numberOfDiceSide)"
        }
        
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let diceNumberTextField = alert.textFields![0]
            let diceSideTextField = alert.textFields![1]
            self.diceRollArray[row] = (numberOfDice: Int(diceNumberTextField.text!)!, numberOfDiceSide: Int(diceSideTextField.text!)!)
            self.diceRollTableView.beginUpdates()
            self.diceRollTableView.reloadData()
            self.diceRollTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        editDiceRoll(row: indexPath.row, title: "Select Roll", message: "Select number and side of dice")
    }


}
