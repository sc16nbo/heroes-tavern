//
//  CustomCreateAbilityScoreController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 08/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the Custom Ability Score Selection View. The user can create and edit his own
/// ability score names and values for the character instance that is in the creation process.
/// The created ability scores are listed on the view of this class.
class CustomCreateAbilityScoreController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var nextButton: UIButton!
    
    var newCharacter = Character()
    let scoreList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    
    /// When tapped Next button this action function performs a segue to the next stage of creation process
    @IBAction func nextButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "goToCustomClass", sender: nextButton)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return scoreList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(scoreList[row])"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newCharacter.abilityScoreList.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "AbilityScoreCell", for: indexPath) as! CustomAbilityScoreCell
        let abilityScoreNameArray = newCharacter.abilityScoreList.keys.sorted()
        let abilityScoreObject = newCharacter.abilityScoreList[(abilityScoreNameArray[indexPath.row])]
        cell.abilityScoreLabel.text = "\(abilityScoreNameArray[indexPath.row])    \(abilityScoreObject!)"
        print(abilityScoreNameArray)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var abilityScoreNameArray = newCharacter.abilityScoreList.keys.sorted()
        editAbilityScore(abilityScoreName: (abilityScoreNameArray[indexPath.row]), row: indexPath.row, title: "Edit Selected Ability Score", message: "Choose a new name and value for your custom ability score")
    }
    
    
    /// This is the action function of the Add Ability Score Button on top right of Custom Abiity Score Selection View's UI. It brings up
    /// an alert with a title, message, a text field for ability score name and one picker view object for ability score value, and finally
    /// Done and Cancel buttons. It gets the entered ability name and value information,
    /// and adds the taken data to the TableView in the Custom Abiity Score Selection View.
    @IBAction func createAbilityScore(_ sender: Any) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(10, inComponent: 0, animated: false)
        vc.view.addSubview(pickerView)
        let alert = UIAlertController(title: "Create A New Ability Score", message: "Choose a name and value for your new ability score", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (textField) in textField.placeholder = "Ability Score Name"}
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: {[weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            //let initialCount = customAbilityScoreList.count
            self.newCharacter.abilityScoreList[(textField?.text)!] = pickerView.selectedRow(inComponent: 0)
            self.tableView.beginUpdates()
            let IndexPathOfLastRow = NSIndexPath(row: self.newCharacter.abilityScoreList.count - 1, section: 0)
            self.tableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
            self.tableView.reloadData()
            self.tableView.endUpdates()
            if self.nextButton.isHidden {
                self.nextButton.isHidden = false
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /// This function is called when the user taps an already existing ability score on
    /// the TableView. It brings up an alert with a title, message, a text field for ability score
    /// name and one picker view object for ability score value, and finally Done and Cancel buttons.
    /// When clicked to Done button, it gets the changed information and updates the respective cell 
    /// data on the TableView.
    ///
    /// - Parameters:
    ///   - abilityScoreName: Name of the ability score that will be edited.
    ///   - row: The row number of the chosen cell on the abilityScoreBonusTableView.
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func editAbilityScore (abilityScoreName: String, row: Int, title: String, message: String){
        let vc = UIViewController()
         vc.preferredContentSize = CGSize(width: 250,height: 300)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(newCharacter.abilityScoreList[abilityScoreName]!, inComponent: 0, animated: false)
        vc.view.addSubview(pickerView)
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (textField) in textField.text = abilityScoreName}
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: {[weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            let value = (self.newCharacter.abilityScoreList[abilityScoreName])!
            self.newCharacter.abilityScoreList[(textField?.text)!] = value
            _ = self.newCharacter.abilityScoreList.removeValue(forKey: abilityScoreName)
            self.newCharacter.abilityScoreList[(textField?.text)!] = pickerView.selectedRow(inComponent: 0)
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
         self.present(alert, animated: true, completion: nil)
        
    }
    

    /// Passing data with the segue to a destinated controller that will use it according to the
    /// identifier of the segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : CustomClassController = segue.destination as! CustomClassController
        destinatedController.newCharacter = newCharacter
    }
}
