//
//  NoteCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 28/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the outlets of cells in Notes sheet of the character
class NoteCell: UITableViewCell {

    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteBodyLabel: UILabel!
    
    /// This function enables the dynamic size change of the note body outlet according to its length
    override func awakeFromNib() {
        super.awakeFromNib()
        noteBodyLabel.sizeToFit()
        noteBodyLabel.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
