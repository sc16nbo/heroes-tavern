//
//  CreateCharacterController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 01/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// Race and Class Selector View's class of the first iteration's D&D RPG character creation process.
/// The user selects class and race of his character from picker views. Respective images and descriptions
/// are presented in the UI.
class CreateCharacterController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    let classImageList =
        [#imageLiteral(resourceName: "barbarian"), #imageLiteral(resourceName: "bard"), #imageLiteral(resourceName: "cleric"), #imageLiteral(resourceName: "druid"), #imageLiteral(resourceName: "fighter"), #imageLiteral(resourceName: "monk"), #imageLiteral(resourceName: "paladin"), #imageLiteral(resourceName: "ranger"), #imageLiteral(resourceName: "rogue"), #imageLiteral(resourceName: "sorcerer"), #imageLiteral(resourceName: "warlock"), #imageLiteral(resourceName: "wizard")]
    
    let classList = ["Barbarian","Bard","Cleric","Druid","Fighter","Monk","Paladin","Ranger","Rogue","Sorcerer","Warlock","Wizard"]
    
    let classDescriptionList = [
        "A fierce warrior of a primitive background who can enter a battle rage, d12, Strength and Constitution, light and medium armor, shields, simple and martial weapons.",
        "An inspiring magician whose power echoes the music of creation, d8, Dexterity & Charisma, light armor, simple weapons, hand crossbows, longswords, rapiers, shortswords",
        "A priestly champion who wields divine magic in service of a higher power, d8, Wisdom & Charisma, light and medium armor, shields, simple weapons",
        "A priest of the Old Faith, wielding the powers of nature— moonlight and plant growth, fire and lightning—and adopting animal forms, d8, Intelligence & Wisdom, light and medium armor(non-metal), shields(non-metal), clubs, daggers, darts, javelins, maces, quarterstaffs, scimitars, sickles, slings, spears",
        "A master of martial combat, skilled with a variety of weapons and armored, d10, Strength & Constitution, all armor, shields, simple and martial weapons",
        "A master of martial arts, skilled with fighting hands and martial monk weapons, d8, Dexterity & Strength (At lvl 14 monk proficiency in all) simple weapons, shortswords",
        "A holy warrior bound to a sacred oath, d10, Strength, Wisdom & Charisma, all armor, shields, simple and martial weapons",
        "A master of ranged combat, one with nature, d10, Dexterity & Strength, light and medium armor, shield, simple weapons, martial weapons",
        "A scoundrel who uses stealth and trickery to overcome obstacles and enemies, d8, Dexterity & Intelligence, light armor, simple weapons, hand crossbows, longswords, rapiers, shortswords",
        "A spellcaster who draws on inherent magic from a gift or bloodline, d6, Constitution & Charisma, daggers, darts, slings, quarterstaffs, light crossbows",
        "A wielder of magic that is derived from a bargain with an extraplanar entity, d8, Wisdom & Charisma, light armor, simple weapons",
        "A scholarly magic-user capable of manipulating the structures of reality, d6, Intelligence & Wisdom, daggers, darts, slings, quarterstaffs, light crossbows"]
    
    let raceList = ["Dragonborn", "Dwarf", "Elf", "Gnome", "Half-Elf", "Half-Orc", "Halfling", "Human", "Tiefling"]
    
    let raceImageList = [#imageLiteral(resourceName: "dragonborn"), #imageLiteral(resourceName: "dwarf"), #imageLiteral(resourceName: "elf"), #imageLiteral(resourceName: "gnome"), #imageLiteral(resourceName: "half-elf"), #imageLiteral(resourceName: "half-orc"), #imageLiteral(resourceName: "halfling"), #imageLiteral(resourceName: "human"), #imageLiteral(resourceName: "tiefling")]
    
    var newCharacter = Character("barbarian","dragonborn")
    
    @IBOutlet weak var creationStepLabel: UILabel!
    @IBOutlet weak var classPicker: UIPickerView!
    @IBOutlet weak var classDescription: UILabel!
    @IBOutlet weak var classImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!

    /// This function is activated with a tap to the Next button in UI.
    /// It changes the content of the whole screen from Class Selection to
    /// Race selection. It also saves the choices of the user and performs
    /// segue to the next step of character creation.
    @IBAction func nextButtonClicked(_ sender: Any) {
        if creationStepLabel.text == "Select Class"
        {
            previousButton.isHidden = false
            creationStepLabel.text = "Select Race"
            classDescription.isHidden = true
            classImage.image = #imageLiteral(resourceName: "dragonborn")
            classPicker.reloadAllComponents()
            classPicker.selectRow(0, inComponent: 0, animated: false)
            newCharacter.charClass = classList[classPicker.selectedRow(inComponent: 0)]
        }
        else if creationStepLabel.text == "Select Race"
        {
            newCharacter.race = raceList[classPicker.selectedRow(inComponent: 0)]
            performSegue(withIdentifier: "goToScoreSelect", sender: nextButton)
        }
    }
    
    /// This action function belongs to the Previous button which becomes unhidden after tapping
    /// Next button once. Clicking the Previous button undoes the context changes the nextButtonClicked
    /// function makes.
    @IBAction func previousButtonClicked(_ sender: Any) {
        previousButton.isHidden = true
        creationStepLabel.text = "Select Class"
        classImage.image = #imageLiteral(resourceName: "barbarian")
        classPicker.reloadAllComponents()
        classPicker.selectRow(0, inComponent: 0, animated: false)
        classDescription.isHidden = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if creationStepLabel.text == "Select Class"{
        return classList.count
        }
        else if creationStepLabel.text == "Select Race"{
        return raceList.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if creationStepLabel.text == "Select Class"{
            return classList[row]
        }
        else if creationStepLabel.text == "Select Race"{
            return raceList[row]
        }
        return ""
    }
    
    /// The selected row changes the D&D character class description and image in the UI.
    /// This function performs these changes.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if creationStepLabel.text == "Select Class"
        {
            classDescription.text = classDescriptionList[row]
            classImage.image = classImageList[row]
        }
        else if creationStepLabel.text == "Select Race"
        {
            classImage.image = raceImageList[row]
        }
    }
    
    /// Passing character data with the segue to a destinated controller that will use it.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : AbilityScoreController = segue.destination as! AbilityScoreController
        destinatedController.newCharacter = newCharacter
    }
    
    /// This function initiates the elements' content and prepares the scene to be used.
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        creationStepLabel.text = "Select Class"
        classDescription.text = classDescriptionList[0]
        classImage.image = #imageLiteral(resourceName: "barbarian")
        previousButton.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
