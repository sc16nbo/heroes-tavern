//
//  CustomItemController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 13/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the Item Selection List View. The user can add any number of items
/// to the character instance that is in the creation process. The creation and edition of items are done
/// in Create Item View and accessed through this class. The created elements are listed on the view of
/// this class.
class CustomItemController: UITableViewController {

    var newCharacter = Character()
    var selectedItemIndex = 0
    @IBOutlet weak var createItemButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        for item in newCharacter.itemList {
            for modifier in item.modifierList{
                newCharacter.abilityScoreList[modifier.key]! += modifier.value
            }
        }
        
        performSegue(withIdentifier: "goToSpellSelect", sender: nextButton)
    }
    
    @IBAction func createItem(_ sender: Any) {
        performSegue(withIdentifier: "goToCreateItem", sender: createItemButton)
            }
    
    
    /// Passing data with the segue to a destinated controller that will use it according to the
    /// identifier of the segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        
        
        if segue.identifier == "goToEditItem"
        {
            let destinatedController : CreateItemController = segue.destination as! CreateItemController
            destinatedController.newCharacter = newCharacter
            print("edit")
            destinatedController.newItem = newCharacter.itemList[selectedItemIndex]
            destinatedController.selectedItemIndex = selectedItemIndex
            destinatedController.createItemButtonTitle = "Finish Editing Item"

        }
        else if segue.identifier == "goToCreateItem"
        {
            let destinatedController : CreateItemController = segue.destination as! CreateItemController
            destinatedController.newCharacter = newCharacter
            print("create")
            destinatedController.createItemButtonTitle = "Create Item"
        }
        else if segue.identifier == "goToSpellSelect"{
            let destinatedController : SpellSelectController = segue.destination as! SpellSelectController
            destinatedController.newCharacter = newCharacter
        }
    }
    
    /// The TableView is updated in this function everytime this view is loaded.
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
           }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newCharacter.itemList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! CustomItemCell
        let itemObject = newCharacter.itemList[indexPath.row]
        cell.itemNameLabel.text = "\(itemObject.name)"
        cell.itemQuantityLabel.text = "\(itemObject.quantity)"
        if itemObject.isMagical {
            cell.itemDetailLabel.text = "\(itemObject.type), Magical Item"
        }
        else {
            cell.itemDetailLabel.text = "\(itemObject.type), Nonmagical Item"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItemIndex = indexPath.row
        performSegue(withIdentifier: "goToEditItem", sender: UITableViewCell.self)
    }
}
