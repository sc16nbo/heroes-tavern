//
//  CreateAttackController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 21/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the attack creation and edition view of the character creation process. It has interface elements to get
/// the necessary information of an attack from the user and saves it to the attackList variable array of the newCharacter instance.
class CreateAttackController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var attackName: UITextField!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var attackType: UITextField!
    @IBOutlet weak var damageTypeLabel: UILabel!
    @IBOutlet weak var attackDamageType: UITextField!
    @IBOutlet weak var attackRollLabel: UILabel!
    @IBOutlet weak var attackRollDiceNumber: UITextField!
    @IBOutlet weak var attackRollDiceSide: UITextField!
    @IBOutlet weak var AttackAbilityLabel: UILabel!
    @IBOutlet weak var attackAbilityPicker: UIPickerView!
    @IBOutlet weak var damageRollLabel: UILabel!
    @IBOutlet weak var damageRollTableView: UITableView!
    @IBOutlet weak var createButton: UIButton!
    
    var newCharacter = Character()
    var newAttack = Attack()
    var selectedAttackIndex = 0
    var createButtonTitle = ""
    var attackRoll = (numberOfDice: 0,numberOfDiceSide: 0)
    var damageRollArray : [(numberOfDice: Int,numberOfDiceSide: Int)] = []
    
    /// This function initiates the outlet values (empty if its a creation view or the previous data
    /// if it is an edition view), writes the placeholder texts for text field objects and gets necessary
    /// information from the new or chosen attack.
    override func viewDidLoad() {
        super.viewDidLoad()
        attackAbilityPicker.delegate = self
        attackAbilityPicker.dataSource = self
        attackName.text = newAttack.name
        attackType.text = newAttack.type
        attackDamageType.text = newAttack.damageType
        attackRollDiceNumber.placeholder = "Number of Dice"
        attackRollDiceNumber.text = "\(newAttack.attackRoll.numberOfDice)"
        attackRollDiceSide.text = "\(newAttack.attackRoll.numberOfDiceSide)"
        attackRollDiceSide.placeholder = "Number of Sides"
        damageRollArray = newAttack.damageRolls
        createButton.setTitle(createButtonTitle, for: .normal)
    }

    
    /// This function takes the information entered by the user and saves them to the Character instance's attackList variable array.
    /// If it is a new attack, it appends to the list, and if it is an editing process, it changes the previous version of the element
    /// in the array. Then it unwinds the segue, returning back to the Attacks and Spells List View of the character creation process.
    @IBAction func createButtonClicked(_ sender: Any) {
        newAttack.name = attackName.text!
        newAttack.type = attackType.text!
        newAttack.damageType = attackDamageType.text!
        newAttack.attackRoll = (numberOfDice: Int(attackRollDiceNumber.text!)! ,numberOfDiceSide: Int(attackRollDiceSide.text!)!)
        newAttack.damageRolls = damageRollArray
        let abilityNameArray = newCharacter.abilityScoreList.keys.sorted()
        newAttack.ability = abilityNameArray[attackAbilityPicker.selectedRow(inComponent: 0)]
        if createButtonTitle == "Create Attack"
        {
            newCharacter.attackList.append(newAttack)
        }
        else
        {
            newCharacter.attackList[selectedAttackIndex] = newAttack
        }
        print(newCharacter.attackList)
        performSegue(withIdentifier: "unwindToAttackList", sender: createButton)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return newCharacter.abilityScoreList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let abilityScoreNameArray = self.newCharacter.abilityScoreList.keys.sorted()
        return abilityScoreNameArray[row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return damageRollArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "damageRollCell", for: indexPath) as! DiceRollCell
        print(damageRollArray[indexPath.row].numberOfDice)
        print(damageRollArray[indexPath.row].numberOfDiceSide)
        cell.attackDamageRollLabel.text = "\(damageRollArray[indexPath.row].numberOfDice)  d\(damageRollArray[indexPath.row].numberOfDiceSide)"
        return cell
    }

    
    /// This is the action function of the Add Damage Dice Roll Button. It brings up
    /// an alert with a title, message, two text fields for getting input and finally
    /// Done and Cancel buttons. It gets the entered dice number and side information,
    /// and adds the taken data to the damageRollTableView in the Create Attack View.
    @IBAction func addDamageRoll(_ sender: Any) {
        let alert = UIAlertController(title: "Add Damage Roll", message: "Select number and side of dice", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (diceNumberTextField) in diceNumberTextField.placeholder = "Number Of Dice"}
        alert.addTextField{ (diceSideTextField) in diceSideTextField.placeholder = "Number Of Sides of One Dice"}
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let diceNumberTextField = alert.textFields![0]
            let diceSideTextField = alert.textFields![1]
            self.damageRollArray.append((numberOfDice: Int(diceNumberTextField.text!)!, numberOfDiceSide: Int(diceSideTextField.text!)!))
            print(self.damageRollArray)
            self.damageRollTableView.beginUpdates()
            let IndexPathOfLastRow = NSIndexPath(row: self.damageRollArray.count - 1, section: 0)
            self.damageRollTableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
            self.damageRollTableView.reloadData()
            self.damageRollTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /// This function is called when the user taps an already existing dice roll on
    /// the damageRollTableView. It brings up an alert with a title, message, two
    /// editable text fields with the already existing information and finally
    /// Done and Cancel buttons. When clicked to Done button, it gets the changed 
    /// dice number and side information and updates the respective cell data on the
    /// damageRollTableView.
    ///
    /// - Parameters:
    ///   - row: The row number of the chosen cell on the damageRollTableView.
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func editDamageRoll (row: Int, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (diceNumberTextField) in diceNumberTextField.placeholder = "Number Of Dice"
            diceNumberTextField.text = "\(self.damageRollArray[row].numberOfDice)"
        }
        alert.addTextField{ (diceSideTextField) in diceSideTextField.placeholder = "Number Of Sides of One Dice"
            diceSideTextField.text = "\(self.damageRollArray[row].numberOfDiceSide)"
        }
        
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let diceNumberTextField = alert.textFields![0]
            let diceSideTextField = alert.textFields![1]
            self.damageRollArray[row] = (numberOfDice: Int(diceNumberTextField.text!)!, numberOfDiceSide: Int(diceSideTextField.text!)!)
            self.damageRollTableView.beginUpdates()
            self.damageRollTableView.reloadData()
            self.damageRollTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        editDamageRoll(row: indexPath.row, title: "Edit Damage Roll", message: "Edit number and side of dice")
    }

}
