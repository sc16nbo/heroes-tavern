//
//  DiceRollViewController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 25/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit


/// This class controls the dice pop up windows being used for ability score checks and spell/attack rolls.
/// It can be reached from the views where mentioned features are located; which are Character Main Tab Page,
/// Spells Tab Page and Attacks/Items Tab Page.
class DiceRollViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closePopUpButton: UIButton!
    @IBOutlet weak var diceOutcomeLabel: UILabel!
    @IBOutlet weak var diceButton: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var diceSummaryLabel: UILabel!
    
    var popUpTitle = ""
    var abilityModifier : Int?
    var diceRollArray : [(numberOfDice: Int,numberOfDiceSide: Int)] = []
    
    /// Function to shape the pop up view's corner with a mask,
    /// write the data of the dice outcome calculation and name to the pop up outlets
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 10
        popUpView.layer.masksToBounds = true
        diceOutcomeLabel.text = ""
        titleLabel.text = "  \(popUpTitle)"
        diceSummaryLabel.text = "\(diceRollArray[0].numberOfDice) d\(diceRollArray[0].numberOfDiceSide) + \(abilityModifier!)"
    }
    
    
    /// This is the function of the dice rolling button in the interface. 
    /// It gets the number and type of the dice from the diceRollArray variable
    /// and gets respective random numbers for every one of them sums up the results
    /// to get the final outcome every time the button is tapped. The outcome is showed
    /// in the diceOutcomeLabel outlet of the interface.
    @IBAction func rollDice(_ sender: Any) {
        var outcome = 0
        var die:UInt32?
        var i = 0
        
        for diceRoll in diceRollArray {
            i = 0
           while i < diceRoll.numberOfDice{
                die = arc4random_uniform(UInt32(diceRoll.numberOfDiceSide))
                outcome += Int(die! + 1)
                i += 1
            }
        }
        
        if abilityModifier != nil {
            outcome += abilityModifier!
        }
        diceOutcomeLabel.text = "\(outcome)"
    }
    
    
    /// This is the action function of the closePopUpButton. When the button is
    /// tapped, the dismiss function is called, closing the pop up and returns to the view
    /// where the pop up was called.
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
