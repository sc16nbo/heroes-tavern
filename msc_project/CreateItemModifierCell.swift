//
//  CreateItemModifierCellTableViewCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 14/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the outlets of cells in item modifier table view in Create Item view.
class CreateItemModifierCell: UITableViewCell {

    @IBOutlet weak var itemModifierCellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
