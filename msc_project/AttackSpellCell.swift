//
//  AttackSpellCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 18/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit


/// This class handles the outlets of cells in attack and item table views in character creation.
class AttackSpellCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var spellSheetNameLabel: UILabel!
    @IBOutlet weak var spellSheetTypeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
