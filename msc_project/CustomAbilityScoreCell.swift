//
//  CustomAbilityScoreCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 08/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the outlets of cells in ability score bonus table view in Custom Race and Class Views
/// also the ability score Table View cells in Custom Ability Score View.
class CustomAbilityScoreCell: UITableViewCell {

    
    @IBOutlet weak var abilityScoreLabel: UILabel!
    @IBOutlet weak var bonusAbilityScoreLabel: UILabel!
    @IBOutlet weak var bonusValue: UILabel!
    @IBOutlet weak var characterSheetAbilityLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
