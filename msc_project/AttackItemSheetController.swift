//
//  AttackItemSheetController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 31/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the Attacks & Items sheet of the character
class AttackItemSheetController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var attackTableView: UITableView!
    @IBOutlet weak var itemTableView: UITableView!
    
    var chosenCharacter = Character()
    var chosenAttack: Attack?
    var chosenItem: Item?
    
    /// Making necessary adjustments to attack and item table views and hiding empty table view cells
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attackTableView.delegate = self
        attackTableView.dataSource = self
        itemTableView.delegate = self
        itemTableView.dataSource = self
        attackTableView.tableFooterView = UIView(frame: .zero)
        itemTableView.tableFooterView = UIView(frame: .zero)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == attackTableView {
            return chosenCharacter.attackList.count
        }
        else {
            return chosenCharacter.itemList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttackItemCell", for: indexPath) as! AttackItemCell
        
        if tableView == attackTableView{
            let attackObject = chosenCharacter.attackList[indexPath.row]
            cell.attackNameLabel.text = "\(attackObject.name)"
            cell.attackTypeLabel.text = "\(attackObject.type)"
        }
        else{
            let itemObject = chosenCharacter.itemList[indexPath.row]
            cell.itemNameLabel.text = "\(itemObject.name)"
            cell.itemTypeLabel.text = "\(itemObject.type)"
        }
        
        
        return cell
    }
    
    /// Function that performs a segue to the proper pop up when a table view cell is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == attackTableView{
            chosenAttack = chosenCharacter.attackList[indexPath.row]
            performSegue(withIdentifier: "goToAttackPopUp", sender: UITableViewCell.self)
        }
        else{
            chosenItem = chosenCharacter.itemList[indexPath.row]
            performSegue(withIdentifier: "goToItemPopUp", sender: UITableViewCell.self)
        }
        
    }
    
    /// Calculating and passing data to the destinated controller that will use it
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAttackPopUp"{
            let destinatedController : AttackPopUpController = segue.destination as! AttackPopUpController
            let abilityModifier = Int((chosenCharacter.abilityScoreList[(chosenAttack?.ability)!]! - 10 ) / 2)
            destinatedController.abilityModifier = abilityModifier
            destinatedController.chosenAttack = chosenAttack!
        }
        if segue.identifier == "goToItemPopUp"{
            let destinatedController : ItemPopUpController = segue.destination as! ItemPopUpController
            destinatedController.chosenItem = chosenItem!
        }
    }

}
