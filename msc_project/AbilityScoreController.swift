//
//  AbilityScoreController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 03/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// The Ability Score Selector class of the first iteration's D&D RPG character creation process.
/// The user can select values of Strength, Dexterity and Intelligence from 3 PickerView object lists.
class AbilityScoreController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var classRaceLabel: UILabel!


    var newCharacter = Character()
    let strScoreList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    let dexScoreList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    let intScoreList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    
    @IBOutlet weak var strPickerView: UIPickerView!
    @IBOutlet weak var dexPickerView: UIPickerView!
    @IBOutlet weak var intPickerView: UIPickerView!

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if pickerView == strPickerView {
            return strScoreList.count
        }
        else if pickerView == dexPickerView {
            return dexScoreList.count
        }
        else if pickerView == intPickerView {
            return intScoreList.count
        }
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == strPickerView {
            return "\(strScoreList[row])"
        }
        else if pickerView == dexPickerView {
            return "\(dexScoreList[row])"
        }
        else if pickerView == intPickerView {
            return "\(intScoreList[row])"
        }
        return " "
    }
    
    /// This function saves the chosen value of the ability scores to the Character instance.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var index = 0
        if pickerView == strPickerView {
            index = 0
            for abilityScore in newCharacter.abilityScoreList{
                
                if (abilityScore.key == "Strength") {
                    newCharacter.abilityScoreList["Strength"] = strScoreList[row]
                    break
                }
            }
                index += 1
        }
        else if pickerView == dexPickerView {
            index = 0
            for abilityScore in newCharacter.abilityScoreList{
                
                if (abilityScore.key == "Dexterity") {
                    newCharacter.abilityScoreList["Dexterity"] = strScoreList[row]
                    break
                }

            }
            index += 1
        }
        else if pickerView == intPickerView {
            index = 0
            for abilityScore in newCharacter.abilityScoreList{
                
                if (abilityScore.key == "Intelligence") {
                    newCharacter.abilityScoreList["Intelligence"] = strScoreList[row]
                    break
                }
            }
            index += 1
        }
    }

    /// Passing character data with the segue to a destinated controller that will use it.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : CharacterNameImageController = segue.destination as! CharacterNameImageController
        destinatedController.newCharacter = newCharacter
    }
    
    /// This function choses the default values of the ability scores and saves them in case the user directly skips them.
    override func viewDidLoad() {
        strPickerView.selectRow(10, inComponent: 0, animated: false)
        dexPickerView.selectRow(10, inComponent: 0, animated: false)
        intPickerView.selectRow(10, inComponent: 0, animated: false)
        newCharacter.abilityScoreList["Strength"] = 10
        newCharacter.abilityScoreList["Dexterity"] = 10
        newCharacter.abilityScoreList["Intelligence"] = 10
        super.viewDidLoad()
        classRaceLabel.text = newCharacter.race + " " + newCharacter.charClass
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
