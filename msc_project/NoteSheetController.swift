//
//  NoteSheetController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 28/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the note sheet of the character.
/// It lists the notes of the character and gives segue to the pop up that helps the 
/// user create and add a new note
class NoteSheetController: UITableViewController {
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var chosenCharacter = Character()
    
    /// This function sends the character data to the note pop up controller for
    /// addition of a new note
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : NotePopUpController = segue.destination as! NotePopUpController
        destinatedController.chosenCharacter = chosenCharacter
    }
    
    
    /// This function  brings the note pop up
    @IBAction func addNote(_ sender: UIBarButtonItem) {
       performSegue(withIdentifier: "goToNotePopUp", sender: addButton)
    }

    /// This function unwinds the segue that was done before and does necessary
    /// changes to the note sheet table view
    @IBAction func unwind(segue:UIStoryboardSegue) {
        if segue.source is NotePopUpController {
            
                self.tableView.beginUpdates()
                let IndexPathOfLastRow = NSIndexPath(row: self.chosenCharacter.noteList.count - 1, section: 0)
                self.tableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
                self.tableView.reloadData()
                self.tableView.endUpdates()
        }
    }
    
    /// This function enables the add note button to be visible
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.rightBarButtonItem = addButton;
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 50
        tableView.tableFooterView = UIView(frame: .zero)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenCharacter.noteList.count
    }
    
    /// To have a dynamic table view cell height automatic dimension is returned
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /// The data of notes of the character is being taken and put to the necessary outlets
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath) as! NoteCell
        cell.noteTitleLabel.text = chosenCharacter.noteList[indexPath.row].title
        cell.noteBodyLabel.text = chosenCharacter.noteList[indexPath.row].body
        return cell
    }
}
