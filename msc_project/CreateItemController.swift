//
//  CreateItemController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 14/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the item creation and edition view of the character creation process. It has interface elements to get
/// the necessary information of an item from the user and saves it to the itemList variable array of the newCharacter instance.
class CreateItemController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    let bonusScoreList = [3,2,1,0,-1,-2,-3]
    var newCharacter = Character()
    var selectedItemIndex: Int?
    var newItem = Item()
    var createItemButtonTitle = ""
    var isMagical = false
    var modifierArray: Dictionary<String,Int> = [:]
    var itemTypeArray = ["Adventuring Gear","Armor","Weapon"]
    
    @IBOutlet weak var ItemNameInput: UITextField!
    @IBOutlet weak var itemQuantityInput: UITextField!
    @IBOutlet weak var createItemButton: UIButton!
    @IBOutlet weak var ItemDescription: UITextView!
    @IBOutlet weak var itemModifierTableView: UITableView!
    @IBOutlet weak var itemTypePickerView: UIPickerView!
    @IBOutlet weak var magicalItemSwitch: UISwitch!
    @IBOutlet weak var magicalItemLabel: UILabel!
    
    
    /// This is the action function of the Add Modifier Button in UI of Create Item View. It brings up
    /// an alert with a title, message, two picker view objects for ability score name and modifier value, and finally
    /// Done and Cancel buttons. It gets the entered ability name and modifier value information,
    /// and adds the taken data to the itemModifierTableView in the Create Item View.
    @IBAction func addItemModifier(_ sender: Any) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        let abilityNamePickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 300))
        abilityNamePickerView.delegate = self
        abilityNamePickerView.dataSource = self
        abilityNamePickerView.tag = 0
        let modifierValuePickerView = UIPickerView(frame: CGRect(x: 125, y: 0, width: 120, height: 300))
        modifierValuePickerView.delegate = self
        modifierValuePickerView.dataSource = self
        modifierValuePickerView.tag = 1
        modifierValuePickerView.selectRow(3, inComponent: 0, animated: false)
        vc.view.addSubview(abilityNamePickerView)
        vc.view.addSubview(modifierValuePickerView)
        let alert = UIAlertController(title: "Choosing Ability Score", message: "Choose an ability score that the item modifies its value", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let abilityNameList = self.newCharacter.abilityScoreList.keys.sorted()
            let selectedAbilityName = abilityNameList[abilityNamePickerView.selectedRow(inComponent: 0)]
            let selectedModifierValue = self.bonusScoreList[modifierValuePickerView.selectedRow(inComponent: 0)]
            if (self.modifierArray[selectedAbilityName] == nil)
            {
                self.modifierArray[selectedAbilityName] = selectedModifierValue
                self.itemModifierTableView.beginUpdates()
                let IndexPathOfLastRow = NSIndexPath(row: self.modifierArray.count - 1, section: 0)
                self.itemModifierTableView.insertRows(at: [IndexPathOfLastRow as IndexPath], with: UITableViewRowAnimation.fade)
                self.itemModifierTableView.reloadData()
                self.itemModifierTableView.endUpdates()
            }
            else
            {
                self.modifierArray[selectedAbilityName] = selectedModifierValue
                self.itemModifierTableView.beginUpdates()
                self.itemModifierTableView.reloadData()
                self.itemModifierTableView.endUpdates()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /// This function assures the input of the text view can only be numerical.
    /// It is called in viewDidLoad to assure the input of quantity of the new item can only
    /// be numerical.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    
    /// This function initiates the outlet values (empty if its a creation view or the previous data
    /// if it is an edition view), writes the placeholder texts for text field objects and gets necessary
    /// information from the new or chosen item.
    override func viewDidLoad() {
        super.viewDidLoad()
        ItemNameInput.placeholder = "Your Item Name"
        createItemButton.setTitle(createItemButtonTitle, for: .normal)
        ItemNameInput.text = newItem.name
        itemQuantityInput.delegate = self
        itemQuantityInput.keyboardType = .numberPad
        itemQuantityInput.text = "\(newItem.quantity)"
        ItemDescription.text = newItem.description
        modifierArray = newItem.modifierList
        itemModifierTableView.beginUpdates()
        itemModifierTableView.reloadData()
        itemModifierTableView.endUpdates()
        itemTypePickerView.dataSource = self
        itemTypePickerView.delegate = self
        itemTypePickerView.tag = 2
        itemTypePickerView.selectRow(itemTypeArray.index(of: newItem.type)!, inComponent: 0, animated: false)
        magicalItemSwitch.isOn = newItem.isMagical
        isMagical = newItem.isMagical
        if newItem.isMagical {
            magicalItemLabel.text = "Magical Item:  Yes"
        }
        else {
           magicalItemLabel.text = "Magical Item:  No"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Passing data with the segue to a destinated controller that will use it.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : CustomItemController = segue.destination as! CustomItemController
        destinatedController.newCharacter = newCharacter
    }
    
    
    /// This is the action function of the magicalItemSwitch in the UI. Altering the
    /// switch also changes the label's text near it.
    @IBAction func changeMagicalProperty(_ sender: Any) {
        if magicalItemSwitch.isOn{
            magicalItemLabel.text = "Magical Item:  Yes"
            isMagical = true
        }
        else{
            magicalItemLabel.text = "Magical Item:  No"
            isMagical = false
        }
    }
    
    
    /// This function takes the information entered by the user and saves them to the Character instance's itemList variable array.
    /// If it is a new item, it appends to the list, and if it is an editing process, it changes the previous version of the element
    /// in the array. Then it performs a segue to the Item List View of the character creation process.
    @IBAction func createItemButtonClicked(_ sender: Any) {
        newItem.name = ItemNameInput.text!
        newItem.quantity = Int(itemQuantityInput.text!)!
        newItem.description = ItemDescription.text
        newItem.modifierList = modifierArray
        newItem.isMagical = isMagical
        newItem.type = itemTypeArray[itemTypePickerView.selectedRow(inComponent: 0)]
        
        if createItemButtonTitle == "Create Item"{
        newCharacter.itemList.append(newItem)
        }
        else{
            newCharacter.itemList[selectedItemIndex!] = newItem
        }
        performSegue(withIdentifier: "goToItemList", sender: self.createItemButton)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag ==  0{
            return newCharacter.abilityScoreList.count
        }
        else if pickerView.tag == 1{
            return bonusScoreList.count
        }
        else if pickerView.tag == 2{
            return itemTypeArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       let abilityScoreNameArray = self.newCharacter.abilityScoreList.keys.sorted()
        if pickerView.tag == 0{
        return abilityScoreNameArray[row]
        }
        else if pickerView.tag == 1{
        return "\(bonusScoreList[row])"
        }
        else if pickerView.tag == 2{
            return "\(itemTypeArray[row])"
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return modifierArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemModifierCell", for: indexPath) as! CreateItemModifierCell
        let abilityScoreNameArray = modifierArray.keys.sorted()
        let abilityScoreName = abilityScoreNameArray[indexPath.row]
        cell.itemModifierCellLabel.text = "\(abilityScoreName)  \(modifierArray[abilityScoreName]!)"
        return cell
    }
    
    
    /// This function is called when the user taps an already existing ability score modifier on
    /// the itemModifierTableView. It brings up an alert with a title, message, two picker view 
    /// objects for ability score name and modifier value, and finally Done and Cancel buttons.
    /// It gets the entered ability name and modifier value information, and adds the taken data
    /// to the itemModifierTableView in the Create Item View. When clicked to Done button, it gets the changed
    /// information and updates the respective cell data on the itemModifierTableView.
    ///
    /// - Parameters:
    ///   - abilityScoreName: Name of the ability score modifier that will be edited.
    ///   - row: The row number of the chosen cell on the itemModifierTableView.
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func editModifier (abilityScoreName: String, row: Int, title: String, message: String){
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 200)
        let modifierValuePickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 200))
        modifierValuePickerView.delegate = self
        modifierValuePickerView.dataSource = self
        modifierValuePickerView.tag = 1
        modifierValuePickerView.selectRow(3, inComponent: 0, animated: false)
        vc.view.addSubview(modifierValuePickerView)
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            
            self.modifierArray[abilityScoreName] = self.bonusScoreList[modifierValuePickerView.selectedRow(inComponent: 0)]
            self.itemModifierTableView.beginUpdates()
            self.itemModifierTableView.reloadData()
            self.itemModifierTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modifierNameList = modifierArray.keys.sorted()
        editModifier(abilityScoreName: (modifierNameList[indexPath.row]), row: indexPath.row, title: "Edit Modifier", message: "Choose a new value for your ability score modifier provided by your item")
    }


}
