//
//  AttackItemCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 31/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the outlets of cells in attack and item table views in character sheet.
class AttackItemCell: UITableViewCell {

    @IBOutlet weak var attackNameLabel: UILabel!
    @IBOutlet weak var attackTypeLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemTypeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
