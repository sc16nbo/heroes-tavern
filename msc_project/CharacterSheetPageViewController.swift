//
//  CharacterSheetPageViewController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 24/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

class CharacterSheetPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    var chosenCharacter = Character()
    
    lazy var pageList: [UIViewController] = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyboard.instantiateViewController(withIdentifier: "MainCharacterSheetVC") as! CharacterSheetController
        let vc2 = storyboard.instantiateViewController(withIdentifier: "SpellSheetVC") as! CharacterSpellSheetController
        let vc3 = storyboard.instantiateViewController(withIdentifier: "EquipmentSheetVC")
        vc1.chosenCharacter = self.chosenCharacter
        vc2.chosenCharacter = self.chosenCharacter
        return [vc1,vc2,vc3]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        if let firstViewController = pageList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = pageList.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        
        guard previousIndex >= 0 else {return nil}
        
        guard pageList.count > previousIndex else {return nil}
        
        return pageList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = pageList.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        
        guard pageList.count != nextIndex else {return nil}
        
        guard pageList.count > nextIndex else {return nil}
        
        return pageList[nextIndex]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
