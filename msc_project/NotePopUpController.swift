//
//  NotePopUpController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 01/08/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class is in charge of the the note creation view controller.
/// It comes into scene as a pop up over the notes sheet and the user can
/// create a new note for his/her character
class NotePopUpController: UIViewController {

    ///Outlets of view controller
    @IBOutlet weak var noteTitleField: UITextField!
    @IBOutlet weak var noteBodyField: UITextView!
    @IBOutlet weak var notePopUpView: UIView!
    
    
    var newNote = Note()
    var chosenCharacter = Character()
    
    /// Function to shape the pop up view's corner with a mask
    override func viewDidLoad() {
        super.viewDidLoad()
        notePopUpView.layer.cornerRadius = 10
        notePopUpView.layer.masksToBounds = true
    }
    
    /// Function to carry the new note data to the note sheet
    /// of the character
    /// - Parameters:
    ///   - segue: The segue object containing information about the view controllers 
    ///   involved in the segue
    ///   - sender: The object that initiated the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : NoteSheetController = segue.destination as! NoteSheetController
        destinatedController.chosenCharacter = chosenCharacter
    }
    
    /// Function to dismiss the pop up
    ///
    /// - Parameter sender: The object that initiated the function
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /// Function that does necessary work when the create note button clicked
    /// It gets the information entered by the user and saves them to the character
    /// - Parameter sender: The object that initiated the function
    @IBAction func createNoteButtonClicked(_ sender: Any) {
        newNote.title = noteTitleField.text!
        newNote.body = noteBodyField.text
        chosenCharacter.noteList.append(newNote)
        performSegue(withIdentifier: "unwindToNoteSheet", sender: UIButton.self)
    }

}
