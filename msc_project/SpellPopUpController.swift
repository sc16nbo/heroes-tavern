//
//  SpellPopUpController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 26/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit


/// This class is in charge of the spell examining view controller.
/// It comes into scene as a pop up over the Spells sheet and the user can
/// see the details of the chosen spell and go to necessary dice roll pop up
class SpellPopUpController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var spellNameLabel: UILabel!
    @IBOutlet weak var spellTypeLabel: UILabel!
    @IBOutlet weak var spellTimeLabel: UILabel!
    @IBOutlet weak var spellRangeLabel: UILabel!
    @IBOutlet weak var spellDurationLabel: UILabel!
    @IBOutlet weak var spellDescriptionLabel: UILabel!
    @IBOutlet weak var spellDescriptionText: UITextView!
    @IBOutlet weak var spellAbilityLabel: UILabel!
    @IBOutlet weak var spellPopUpView: UIView!
    @IBOutlet weak var spellDiceRollTableView: UITableView!
    
    var chosenSpell = Spell()
    var diceRollArray : [(numberOfDice: Int,numberOfDiceSide: Int)] = []
    var abilityModifier : Int?
    
    
    /// Function to dismiss the pop up
    ///
    /// - Parameter sender: The object that initiated the function
    @IBAction func closeSpellPopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /// Function to shape the pop up view's corner with a mask, making necessary adjustments to the spell dice roll tableview
    /// write the data of the chosen spell to the pop up outlets
    /// and hide empty table view cells
    override func viewDidLoad() {
        super.viewDidLoad()
        spellDiceRollTableView.delegate = self
        spellDiceRollTableView.dataSource = self
        spellPopUpView.layer.cornerRadius = 10
        spellPopUpView.layer.masksToBounds = true
        spellNameLabel.text = chosenSpell.name
        spellTypeLabel.text = chosenSpell.type
        spellTimeLabel.text = chosenSpell.time
        spellRangeLabel.text = "Range: \(chosenSpell.range)"
        spellDurationLabel.text = "Duration: \(chosenSpell.duration)"
        spellDescriptionText.text = chosenSpell.description
        spellAbilityLabel.text = "Spellcasting Ability: \(chosenSpell.ability)"
        spellDiceRollTableView.tableFooterView = UIView(frame: .zero)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenSpell.diceRolls.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiceRollCell", for: indexPath) as! DiceRollCell
        
        let diceRollObject = chosenSpell.diceRolls[indexPath.row]
        cell.spellSheetDiceRollLabel.text = "\(diceRollObject.numberOfDice) d\(diceRollObject.numberOfDiceSide)"
        
        return cell
    }
    
    /// This function gets the dice roll numbers and name information from the chosen spell and transfers it to the dice rolling pop up window.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDiceRollPopUp" {
            let destinatedController : DiceRollViewController = segue.destination as! DiceRollViewController
                destinatedController.diceRollArray = diceRollArray
            destinatedController.popUpTitle = "\(chosenSpell.name)"
            destinatedController.abilityModifier = abilityModifier
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        diceRollArray = [(numberOfDice: chosenSpell.diceRolls[indexPath.row].numberOfDice,numberOfDiceSide: chosenSpell.diceRolls[indexPath.row].numberOfDiceSide)]
        
        performSegue(withIdentifier: "goToDiceRollPopUp", sender: self)
    }
}
