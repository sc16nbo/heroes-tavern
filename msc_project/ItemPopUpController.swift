//
//  ItemPopUpController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 01/08/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class is in charge of the item examining view controller.
/// It comes into scene as a pop up over the attack & items sheet and the user can
/// see the details of the chosen item
class ItemPopUpController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemTypeMagicLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    @IBOutlet weak var itemDescription: UITextView!
    @IBOutlet weak var itemModifierTableView: UITableView!
    @IBOutlet weak var itemPopUpView: UIView!
    
    var chosenItem = Item()
    var isMagical = "Nonmagical Item"
    
    /// Function to shape the pop up view's corner with a mask,
    /// write the data of the chosen item to the pop up outlets
    /// and hide empty table view cells
    override func viewDidLoad() {
        super.viewDidLoad()
        itemPopUpView.layer.cornerRadius = 10
        itemPopUpView.layer.masksToBounds = true
        itemModifierTableView.delegate = self
        itemModifierTableView.dataSource = self
        itemNameLabel.text = chosenItem.name
        if (chosenItem.isMagical){
            isMagical = "Magical Item"
        }
        itemTypeMagicLabel.text = "\(chosenItem.type), \(isMagical)"
        itemQuantityLabel.text = "Quantity: \(chosenItem.quantity)"
        itemDescription.text = chosenItem.description
        itemDescription.isUserInteractionEnabled = false
        itemModifierTableView.tableFooterView = UIView(frame: .zero)
    }

    
    /// Function to dismiss the pop up
    ///
    /// - Parameter sender: The object that initiated the function
    @IBAction func closePopUp(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (chosenItem.modifierList.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    /// Gets ability modifiers from the item data and puts them in cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! CustomItemCell
        let modifierArray = chosenItem.modifierList.keys.sorted()
        cell.itemPopUpModifierLabel.text = "\(modifierArray[indexPath.row])   \(chosenItem.modifierList[(modifierArray[indexPath.row])]!)"

        return cell
    }

}
