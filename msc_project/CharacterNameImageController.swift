//
//  CharacterNameImageController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 03/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the Name and Image Selection View. The user can choose a name and
/// reference image from device gallery for his own character instance that is in the creation
/// process. It also includes a random name generator that gets information from an online generator.
class CharacterNameImageController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var newCharacter : Character?
    
    @IBOutlet weak var chosenImageViewer: UIImageView!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var charSummary: UILabel!
    @IBOutlet weak var nameInput: UITextField!
    
    
    /// This action function asks for access to the device gallery
    /// for the user to select a reference image for the character instance.
    @IBAction func chooseImage(_ sender: Any) {
        
        chooseImageButton.isHidden = true
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        chosenImageViewer.image = chosenImage
        newCharacter?.charImage = chosenImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    /// Randomise button action function. When tapped, it makes a HTTP connection,
    /// gets the first random name from the site content and writes it to the text field.
    @IBAction func randomiseButtonClicked(_ sender: Any) {
        let myURLString = "https://www.fantasynamegen.com/human/short/"
        guard let myURL = URL(string: myURLString) else {
            print("Error: \(myURLString) doesn't seem to be a valid URL")
            return
        }
        
        do {
            let myHTMLString = try String(contentsOf: myURL, encoding: .ascii)
            
            let open = myHTMLString.range(of: "<li>")!
            let close = myHTMLString.range(of: "</li>")!
            let range = open.upperBound..<close.lowerBound
            let enclosed = myHTMLString.substring(with: range)
            print(enclosed)
            nameInput.text = enclosed
        } catch let error {
            print("Error: \(error)")
        }
    }

    /// This action function belongs to the Choose Button. When activated,
    /// it saves the name to the character instance and writes the character summary on the screen.
    @IBAction func chooseButtonClicked(_ sender: Any) {
        newCharacter?.name = nameInput.text!
        charSummary.text = "Name: \((newCharacter?.name)!) , Race: \((newCharacter?.race)!) , Class: \((newCharacter?.charClass)!) , Ability Scores: \((newCharacter?.abilityScoreList)!)"
    }
    
    
    /// Passing data with the segue to a destinated controller that will use it according to the
    /// identifier of the segue. It also alerts the destination that a new character has been created.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCharacterList" {
        let destinatedController : CharacterListController = segue.destination as! CharacterListController
        destinatedController.newCharacter = newCharacter!
        destinatedController.didCreateNewCharacter = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}
