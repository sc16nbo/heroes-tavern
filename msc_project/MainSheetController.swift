//
//  MainSheetController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 01/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the first tab page of the character sheet feature. It shows the image, name, race, class
/// hit points, ability scores of the character. The user can change the character's current and maximum hit points
/// and perform ability checks from the UI view of this class.
class MainSheetController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    
    @IBOutlet weak var characterImageViewer: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var characterRaceClassLabel: UILabel!
    @IBOutlet weak var abilityScoreTableView: UITableView!
    @IBOutlet weak var greenHealthBar: UIView!
    @IBOutlet weak var redHealthBar: UIView!
    @IBOutlet weak var changeHPButton: UIButton!
    
    var chosenCharacter = Character()
    var abilityScoreNameList = ""
    var abilityScoreValueList = ""
    var chosenAbility = ""
    var abilityModifier = 0
    var currentHP = 100
    var maxHP = 100
    
    /// When a navigation bar button added to one of the tab view UIs under the tab controller,
    /// it is added to all of them. This function makes sure the added button does not appear on
    /// the UI view of this class. It also loads the character image, name race and class to the
    /// appropriate outlets.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.rightBarButtonItem = nil;
        characterImageViewer.image = chosenCharacter.charImage
        characterNameLabel.text = chosenCharacter.name
        characterRaceClassLabel.text = "Race: \(chosenCharacter.race)   Class: \(chosenCharacter.charClass)"
    }

    /// This function manages the abilityScoreTableView and makes sure the 
    /// empty cells stay hidden.
    override func viewDidLoad() {
        super.viewDidLoad()
        abilityScoreTableView.delegate = self
        abilityScoreTableView.dataSource = self
        abilityScoreTableView.tableFooterView = UIView(frame: .zero)
    }
    
    /// This function assures the input of the text view can only be numerical.
    /// It is called in changeHP function to assure the current and maximum HP input can only
    /// be numerical.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    /// This function appears when the user taps on the HP bar in the UI view of this class. It brings up
    /// an alert with two text fields for current and maximum HP input, and Done and Cancel buttons.
    /// if the entered max HP is smaller than the current HP value it gives another alert informing that
    /// it is wrong and dismisses both the alerts. Otherwise, it changes the HP values and the size of the
    /// green bar in the UI that indicates how the amount of HP the character has accordingly.
    @IBAction func changeHP(_ sender: Any) {
        let alert = UIAlertController(title: "Health Points", message: "Change health points of your character", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField{ (currentHPTextField) in currentHPTextField.placeholder = "Current Health"
            currentHPTextField.delegate = self
            currentHPTextField.keyboardType = .numberPad
        if self.changeHPButton.currentTitle != "HP/HP"{
            currentHPTextField.text = String(self.currentHP)}}
        alert.addTextField{ (maxHPTextField) in maxHPTextField.placeholder = "Maximum Health"
            maxHPTextField.delegate = self
            maxHPTextField.keyboardType = .numberPad
        if self.changeHPButton.currentTitle != "HP/HP"{
            maxHPTextField.text = String(self.maxHP)}}
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            let currentHPTextField = alert.textFields![0]
            let maxHPTextField = alert.textFields![1]
            if Int(maxHPTextField.text!)! < Int(currentHPTextField.text!)! {
                self.createAlert(title: "Invalid HP", message: "Current HP of the character cannot be more than his/her max HP")
            }
            else{
                self.currentHP = Int(currentHPTextField.text!)!
                self.maxHP = Int(maxHPTextField.text!)!
                var newHPBarLength = Float(currentHPTextField.text!)! / Float(maxHPTextField.text!)!
                newHPBarLength = newHPBarLength * Float(self.redHealthBar.frame.width)
                self.greenHealthBar.frame = CGRect(x: 0, y: 0, width: Int(newHPBarLength), height: Int(self.greenHealthBar.frame.height))
                self.changeHPButton.setTitle("\(currentHPTextField.text!)/\(maxHPTextField.text!)", for: .normal)
            }
                    }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// This function is used to create a system alert.
    ///
    /// - Parameters:
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func createAlert (title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return chosenCharacter.abilityScoreList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "AbilityScoreCell", for: indexPath) as! CustomAbilityScoreCell
        let abilityScoreNameArray = chosenCharacter.abilityScoreList.keys.sorted()
        cell.characterSheetAbilityLabel.text = "\(abilityScoreNameArray[indexPath.row])  \(chosenCharacter.abilityScoreList[abilityScoreNameArray[indexPath.row]]!)"
        return cell
    }
    
    /// Selecting a row in the abilityScoreTableView brings an ability check pop up.
    /// This function performs necessary data calculations beforehand and performs the
    /// segue to the ability check pop up.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let abilityScoreNameArray = chosenCharacter.abilityScoreList.keys.sorted()
        chosenAbility = abilityScoreNameArray[indexPath.row]
        abilityModifier = Int((chosenCharacter.abilityScoreList[chosenAbility]! - 10 ) / 2)
        performSegue(withIdentifier: "goToAbilityCheckPopUp", sender: self)
    }
    
    /// Passing ability score, modifier and UI element data with the segue to a destinated controller that will use it.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToAbilityCheckPopUp"
        {
            let destinatedController : DiceRollViewController = segue.destination as! DiceRollViewController
            destinatedController.popUpTitle = "\(chosenAbility) Check"
            destinatedController.abilityModifier = abilityModifier
            destinatedController.diceRollArray = [(numberOfDice: 1,numberOfDiceSide: 20)]
        }
    }


}
