//
//  AttackPopUpController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 31/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class is in charge of the attack examining view controller.
/// It comes into scene as a pop up over the attack & items sheet and the user can
/// see the details of the chosen attack and go to necessary dice roll pop up
class AttackPopUpController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var attackNameLabel: UILabel!
    @IBOutlet weak var attackTypeLabel: UILabel!
    @IBOutlet weak var damageTypeLabel: UILabel!
    @IBOutlet weak var attackAbilityLabel: UILabel!
    @IBOutlet weak var diceRollLabel: UILabel!
    @IBOutlet weak var diceRollTableView: UITableView!
    @IBOutlet weak var attackPopUpView: UIView!
    
    var chosenAttack = Attack()
    let sectionNameArray = ["Attack Roll","Damage Rolls"]
    var diceRollArray : [(numberOfDice: Int,numberOfDiceSide: Int)] = []
    var abilityModifier : Int?
    var popUpTitle  : String?
    
    /// Function to shape the pop up view's corner with a mask,
    /// write the data of the chosen attack to the pop up outlets
    /// and hide empty table view cells
    override func viewDidLoad() {
        super.viewDidLoad()
        attackPopUpView.layer.cornerRadius = 10
        attackPopUpView.layer.masksToBounds = true
        attackNameLabel.text = chosenAttack.name
        attackTypeLabel.text = chosenAttack.type
        damageTypeLabel.text = chosenAttack.damageType
        attackAbilityLabel.text = chosenAttack.ability
        diceRollTableView.dataSource = self
        diceRollTableView.delegate = self
        diceRollTableView.tableFooterView = UIView(frame: .zero)
    }
    
    /// Function to dismiss the pop up
    ///
    /// - Parameter sender: The object that initiated the function
    @IBAction func closeAttackPopUp(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    

    /// This table view has two sections : Attack Roll and the Damage Rolls
    ///
    /// - Parameter tableView: The dice roll table view
    /// - Returns: the number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    

    /// Getting the section header names from the created array above and using them
    ///
    /// - Parameters:
    ///   - tableView: The dice roll table view
    ///   - section: one of the 2 sections
    /// - Returns: section header title
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionNameArray[section]
    }
    
    /// Attack roll section has only one row
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
           return 1
        }
        else {
            return chosenAttack.damageRolls.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    /// This function gets the number of dice and dice side values from the chosen 
    /// attack data and puts them on the outlets of the cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiceRollCell", for: indexPath) as! DiceRollCell
        if indexPath.section == 0{
            let diceRollObject = chosenAttack.attackRoll
            cell.attackSheetDiceRollLabel.text = "\(diceRollObject.numberOfDice) d\(diceRollObject.numberOfDiceSide)"
        }
        else {
            let diceRollObject = chosenAttack.damageRolls[indexPath.row]
            cell.attackSheetDiceRollLabel.text = "\(diceRollObject.numberOfDice) d\(diceRollObject.numberOfDiceSide)"
        }
        
        return cell
    }
    
    /// This function passes data to destinated controller, in this case dice roll popup
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDiceRollPopUp" {
            let destinatedController : DiceRollViewController = segue.destination as! DiceRollViewController
            destinatedController.diceRollArray = diceRollArray
            destinatedController.popUpTitle = popUpTitle!
            destinatedController.abilityModifier = abilityModifier
        }
    }
    
    /// When a dice roll is selected the necessary changes are made before passing them
    /// to the dice roll pop up and performs the segue to the dice roll pop up
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            diceRollArray = [(numberOfDice: chosenAttack.attackRoll.numberOfDice,numberOfDiceSide: chosenAttack.attackRoll.numberOfDiceSide)]
            popUpTitle = "\(chosenAttack.name) Attack Roll"
        }
        else {
            diceRollArray = [(numberOfDice: chosenAttack.damageRolls[indexPath.row].numberOfDice,numberOfDiceSide: chosenAttack.damageRolls[indexPath.row].numberOfDiceSide)]
            popUpTitle = "\(chosenAttack.name) Damage Roll"
        }
        performSegue(withIdentifier: "goToDiceRollPopUp", sender: self)
    }
}
