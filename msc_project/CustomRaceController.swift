//
//  CustomRaceController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 11/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class controls the Custom Race Selection View. The user can create his own
/// race for the character instance that is in the creation process and add ability bonuses.
/// The created modifiers are listed on the view of this class.
class CustomRaceController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    let bonusScoreList = [3,2,1,0,-1,-2,-3]
    var newCharacter = Character()
    var  tempCharacter = Character()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chooseBonusValuesPromptLabel: UILabel!
    @IBOutlet weak var abilityScoreBonusTableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var chooseNameButton: UIButton!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    /// This action function triggers when the Choose button is clicked and it makes the bonus value table
    /// and related prompt text visible.
    @IBAction func chooseButtonClicked(_ sender: Any) {
            abilityScoreBonusTableView.isHidden = false
            chooseBonusValuesPromptLabel.text = "Choose bonus values that \(textField.text!) race provides (if any):"
            chooseBonusValuesPromptLabel.isHidden = false
    }
    
    
    /// This action function belongs to the Next button in the UI. When tapped it saves the chosen race information
    /// and performs a segue to the next stage of creation process.
    @IBAction func nextButtonClicked(_ sender: Any) {
        tempCharacter.race = textField.text!
        performSegue(withIdentifier: "goToCustomItem", sender: nextButton)
    }
    
    /// Passing data with the segue to a destinated controller that will use it according to the
    /// identifier of the segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinatedController : CustomItemController = segue.destination as! CustomItemController
        destinatedController.newCharacter = tempCharacter
    }
    
    /// This function initiates the outlet placeholders and titles, and manages tableview objects.
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Choose a name for your custom race:"
        textField.placeholder = "Your Race Name"
        chooseNameButton.setTitle("Choose Race Name", for: UIControlState.normal)
        abilityScoreBonusTableView.delegate = self
        abilityScoreBonusTableView.dataSource = self
        chooseBonusValuesPromptLabel.isHidden = true
        abilityScoreBonusTableView.isHidden = true
        tempCharacter = newCharacter
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bonusScoreList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(bonusScoreList[row])"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newCharacter.abilityScoreList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCell(withIdentifier: "RaceBonusAbilityScoreCell", for: indexPath) as! CustomAbilityScoreCell
        var abilityScoreNameArray = newCharacter.abilityScoreList.keys.sorted()
        var tempAbilityScoreNameArray = tempCharacter.abilityScoreList.keys.sorted()
        cell.bonusAbilityScoreLabel.text = "\(abilityScoreNameArray[indexPath.row])"
        cell.bonusValue.text = "\(tempCharacter.abilityScoreList[tempAbilityScoreNameArray[indexPath.row]]! - newCharacter.abilityScoreList[abilityScoreNameArray[indexPath.row]]!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tempAbilityScoreNameArray = tempCharacter.abilityScoreList.keys.sorted()
        editAbilityScore(abilityScoreName: (tempAbilityScoreNameArray[indexPath.row]), row: indexPath.row, title: "Edit Selected Ability Score Bonus", message: "Choose a bonus value for your ability score provided by your custom race")
    }
    
    /// This function is called when the user taps an already existing ability score bonus on
    /// the abilityScoreBonusTableView. It brings up an alert with a title, message, a picker view
    /// object for ability score bonus value, and finally Done and Cancel buttons.
    /// It gets the entered ability name and bonus value information, and adds the taken data
    /// to the abilityScoreBonusTableView in the Custom Race Select View. When clicked to Done button, it gets the changed
    /// information and updates the respective cell data on the abilityScoreBonusTableView.
    ///
    /// - Parameters:
    ///   - abilityScoreName: Name of the ability score bonus that will be edited.
    ///   - row: The row number of the chosen cell on the abilityScoreBonusTableView.
    ///   - title: The title of the alert pop up.
    ///   - message: The message of the alert pop up.
    func editAbilityScore (abilityScoreName: String, row: Int, title: String, message: String){
        if(tempCharacter.abilityScoreList[abilityScoreName] != newCharacter.abilityScoreList[abilityScoreName]){
            tempCharacter.abilityScoreList[abilityScoreName] = newCharacter.abilityScoreList[abilityScoreName]
        }
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 200)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(3, inComponent: 0, animated: false)
        vc.view.addSubview(pickerView)
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: { (_) in
            
            self.tempCharacter.abilityScoreList[abilityScoreName]! += self.bonusScoreList[pickerView.selectedRow(inComponent: 0)]
            print(self.tempCharacter.abilityScoreList[abilityScoreName]!)
            self.abilityScoreBonusTableView.beginUpdates()
            self.abilityScoreBonusTableView.reloadData()
            self.abilityScoreBonusTableView.endUpdates()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

}
