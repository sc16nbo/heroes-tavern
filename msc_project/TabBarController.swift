//
//  TabBarController.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 28/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class is in charge of managing the tab bar controller of the character sheet
/// and the passage of chosen character data between the character list and the pages of character sheet pages
class TabBarController: UITabBarController {

    var chosenCharacter = Character()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barViewControllers = self.viewControllers
        let vc1 = barViewControllers![0] as! MainSheetController
        vc1.chosenCharacter = self.chosenCharacter
        let vc2 = barViewControllers![1] as! SpellSheetController
        vc2.chosenCharacter = self.chosenCharacter
        let vc3 = barViewControllers![2] as! AttackItemSheetController
        vc3.chosenCharacter = self.chosenCharacter
        let vc4 = barViewControllers![3] as! NoteSheetController
        vc4.chosenCharacter = self.chosenCharacter
    }
}
