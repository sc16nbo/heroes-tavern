//
//  DiceRollCell.swift
//  msc_project
//
//  Created by Nihat Ozcan [sc16nbo] on 21/07/2017.
//  Copyright © 2017 Nihat Ozcan [sc16nbo]. All rights reserved.
//

import UIKit

/// This class handles the outlets of cells in TableView objects that include dice roll information.
class DiceRollCell: UITableViewCell {

    @IBOutlet weak var diceRollLabel: UILabel!
    @IBOutlet weak var attackDamageRollLabel: UILabel!
    @IBOutlet weak var spellSheetDiceRollLabel: UILabel!
    @IBOutlet weak var attackSheetDiceRollLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
